(ns hwo2014bot.trackparsing)

(defn bend? [piece] (not (:length piece)))
(defn straight? [piece] (not (bend? piece)))

(defn straight-left-from-this [pieceindex trackcycle]
	(count (take-while straight? (drop pieceindex trackcycle))))

(defn start-index-of-longest [tracklength trackcycle]
	(first (apply max-key second
		(map-indexed vector 
			(map straight-left-from-this 
				(range tracklength) (repeat trackcycle))))))